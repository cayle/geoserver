const view = new ol.View({
    center: [0, 0],
    zoom: 2
});

const map = new ol.Map({
    layers: [
        new ol.layer.Tile({
            source: new ol.source.OSM()
        })
    ],
    target: 'map',
    view: view
});


