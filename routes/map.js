const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const bus = require('../lib/messageStream');

//router.post('/', auth.requireUser);

router.get('/', function(req, res, next) {
    res.render('map', { title: 'Map' });
});

module.exports = router;
