const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');

/* GET users listing. */
router.get('/', function(req, res) {
  res.send('respond with a resource');
});

router.post('/login', (req,res) => {
    const secretToken = 'password';
    const user = req.body;
    const token = jwt.sign({id: user.id, name: user.name}, secretToken);
    return res.json({token:token});
});

module.exports = router;

