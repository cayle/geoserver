const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const bus = require('../lib/messageStream');

//router.post('/', auth.requireUser);

router.post('/', function(req, res, next) {
    const payload = req.body;
    if (payload) {
        bus.postMessage('position', payload);
        return res.status(200).json({"status": true});
    }
    return res.status(400).json({ status: false, message: "No payload"});
});

module.exports = router;
