const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const lessMiddleware = require('less-middleware');
const jwt = require('express-jwt');
const bus = require('./lib/messageStream');
const app = express();

// view engine setup
bus.postSystemMessage('info', 'Setting up view engine');
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

bus.postSystemMessage('info', 'Setting up middleware');
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(lessMiddleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

bus.postSystemMessage('info', 'Adding routes');
app.use('/', require('./routes/index'));
app.use('/users', require('./routes/users'));

//app.use(jwt({secret: 'password'}).unless({ path: ['/users/login']}));
app.use('/geo', require('./routes/geo'));
app.use('/map', require('./routes/map'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
