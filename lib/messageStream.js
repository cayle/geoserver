const EventEmitter = require('events');

class MessageBus extends EventEmitter {
    postMessage(type, data) {
        this.emit(type, data);
    }
    
    postSystemMessage(level, message, meta = {}) {
        this.emit('system', { level, message, meta });
    }
}


const messages = new MessageBus();

module.exports = messages;

