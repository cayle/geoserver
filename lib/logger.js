const winston = require('winston');
const bus = require('./messageStream');

const logger = winston.createLogger({
    level: 'debug',
    exitOnError: false,
    transports: [
        new winston.transports.Console(),
    ],
    format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.colorize(),
        winston.format.printf(info => `${info.timestamp} [${info.level}]: ${info.message}`)
    )
});

bus.on('position', info => {
    logger.info(`New position received: ${info.lat}, ${info.lon}`);
});

bus.on('system', info => {
    logger.log(info.level || 'info', info.message, info.meta);
});

module.export = logger;
